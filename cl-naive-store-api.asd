(defsystem "cl-naive-store-api"
  :description "Exposes an cl-naives-store with http."
  :version "2023.10.18"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on ("split-sequence"
               "bordeaux-threads"
               "drakma"
               "woo"
               "cl-naive-store")
  :components ((:file "packages")
	       (:file "common" :depends-on ("packages"))
	       (:file "api" :depends-on ("common"))
	       (:file "examples" :depends-on ("api"))))

