(in-package :cl-naive-store-api)

;;For docker you need to bind to 0.0.0.0 and not 127.0.0.1
;;google it for more info
(defparameter *bound-ip* "127.0.0.1")

(defparameter *handlers* (make-hash-table :test 'equalp))

