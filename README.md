# cl-naive-store-api

Lets you access cl-naive-store data stores via http.

So the basic design should allow you to use any lisp http server but for now it supports woo out of the box.

Selected woo because I want to learn it and because I think it should be light and fast.

This is still very raw, but you can run-example and go to http://localhost:5000/simple-store/simple-collection to see how it works.

Check issues to see current work etc.

