(in-package :cl-naive-store-api)

(defun run-example ()
  (let ((universe (make-instance 
		   'cl-naive-store:universe
		   :location "~/data-universe/" ;Setting the location on disk.
		   :store-class 'cl-naive-store:store)))


    (let* (;;Create a store and add it to the universe
	   (store (cl-naive-store:add-store
		   universe
		   (make-instance 'cl-naive-store:store
				  :name "simple-store"
				  :collection-class 'cl-naive-store:collection)))
	   
	   ;;Create a collection and add it to the store
	   (collection 
	    (cl-naive-store:add-collection store
					   (make-instance 'cl-naive-store:collection
							  :name "simple-collection"))))
      
      ;;Add some objects to the collection
      (cl-naive-store:persist-document collection (list :name "Piet" :surname "Gieter" :id 123))
      (cl-naive-store:persist-document collection (list :name "Sannie" :surname "Gieter" :id 321))
      (cl-naive-store:persist-document collection (list :name "Koos" :surname "Van" :id 999))

      (cl-naive-store-api::start-api-listener nil :collections (list collection)))))



#|

(time (cl-naive-store-api::query "simple-store" "simple-collection"
					 '(lambda (doc) (equalp(cl-getx:getx doc :id) 999))
					 :cache-p t :force-cache-update-p nil))
|#
