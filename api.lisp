(in-package :cl-naive-store-api)

(defclass listener ()
  ((url :initarg :url
	:initform "localhost"
	:accessor url)
   (port :initarg :port
	 :initform 5000
	 :accessor port))
  (:documentation "Used to specialize start listerner."))

(defparameter *listener* (make-instance 'listener))

(defclass handler ()
  ((collection :initarg :collection
	       :accessor collection
	       :initform nil
	       :documentation "cl-naive-store collection.")
   (path :initarg :path
	 :accessor path
	 :initform nil
	 :documentation "Url to map to.")
   (cache :initarg :cache
	  :initform (make-hash-table :test 'equal)
	  :accessor cache))
  
  (:documentation "A collection of objects of a specific data-type."))

(defgeneric register-collection (collection &key path apis &allow-other-keys)
  (:documentation "Expose a collection. You dont want to expose all collections by default because fo security risks."))

(defmethod register-collection (collection &key path handler &allow-other-keys)
  (setf (gethash (list (cl-naive-store:name (cl-naive-store:store collection))
		       (cl-naive-store:name collection))
		 *handlers*)
	(or handler
	    (make-instance
	     'handler :collection collection
	     :path (or path
		       (format nil "/~A/~A"
			       (cl-naive-store:name (cl-naive-store:store collection))
			       (cl-naive-store:name collection)))))))

(defgeneric handle-request (listener request &key &allow-other-keys))

(defun param (name params)
  (second (find name params :test 'equal :key (lambda (pair)
					       (car pair)))))

(defun query-string-params (query-string)
  (let ((query-string (split-sequence:split-sequence #\& query-string))
	(params))
    
    (dolist (param query-string)
      (when param
	(let ((pair (split-sequence:split-sequence #\= param)))
	  (push pair params))))
    params))

(defgeneric handle-api (listener handler api &key request params &allow-other-keys))

(defun hash-values (hash-table)
  (when hash-table
    (loop for value being the hash-values of hash-table collect value)))

(defun read-body (request)
  (let ((body
	 (make-array (getf request :content-length)
		     :element-type '(unsigned-byte 8))))
    (read-sequence body (getf request :raw-body))
    body))

(defmethod handle-api (listener handler api &key request params &allow-other-keys)
  (declare (ignorable params))

  (let* ((body-octets (read-body request))
	 (body (if body-octets
		   (babel:octets-to-string body-octets)))
	 
	 (query-hash (if body
			 (ironclad:octets-to-integer
			  (ironclad:digest-sequence :tiger body-octets))))
	 (query (if body
		    (getf
		     (eval (read-from-string body))
		     :query)))
	 (data (if (or (equalp (param "force-cache-update-p" params) "true")
		       (not (equalp (param "cache-p" params) "true")))
		   (if (getf request :request-method)
			 (cl-naive-store:query-data (collection handler)
						    :query query)
			 (cl-naive-store:documents (collection handler)))))
	 (data-string)
	 (cached-result-p))

    ;;TODO: What to do about cached NIL?
    (if (or (equalp (param "cache-p" params) "true")
	    (equalp (param "force-cache-update-p" params) "true"))
	(let ((cached-data (gethash query-hash (cache handler))))
	  (if (and (not (equalp (param "force-cache-update-p" params) "true"))
		   cached-data)
	      (progn
		(setf cached-result-p t)
		(setf data-string cached-data))
	      (progn
		(setf data-string (format nil "~S" (if (hash-table-p data)
						       (hash-values data)
						       data)))
		(setf (gethash query-hash (cache handler))
		      data-string))))
	
	(setf data-string (format nil "~S" (if (hash-table-p data)
					       (hash-values data)
					       data))))
    
    (list 200
	  (list :content-type "text/plain;char-set=utf-8")
	  (list (format nil "(:cached-result-p ~A ~%  :data ~A)" cached-result-p data-string)))

    ))

(defmethod handle-request (listener request &key &allow-other-keys)  
  (let ((handler (gethash (cdr (split-sequence:split-sequence
				#\/
				(getf request :path-info)))
			  *handlers*)))
    (unless handler 
      (return-from handle-request
	(list 401
	      (list :content-type "text/plain")
	      (list (format nil "No handler for ~A" (getf request :REQUEST-URI))))))

    (when handler
      (let* ((params (query-string-params (getf request :query-string)))
	     (api (getf params :api)))        
	(handle-api listener handler (if api (second api)) :request request :params params)))))

(defgeneric start-api-listerner (listener &key &allow-other-keys))

(defmethod start-api-listener (listener &key collections &allow-other-keys)
  (when listener
    (setf *listener* listener))
 
  (dolist (collection collections)
    (register-collection collection))

  (bt:make-thread
   (lambda ()
     (woo:run 
      (lambda (env)	
	(let ((response (handle-request listener env)))
	  (if response
	      response
	      (list 401
		    (list :content-type "text/plain")
		    (list "Resource not found")))))
      
      :address *bound-ip*
      :port (port *listener*)))))

(defun query (store collection query &key cache-p force-cache-update-p)
  (drakma:http-request
	      (format nil "http://~A:~A/~A/~A"
		      (url *listener*)
		      (port *listener*)
		      store
		      collection)
	      :method :post
	      :parameters (list (cons "cache-p" (if cache-p
						    "true"
						    "false"))
				(cons "force-cache-update-p" (if force-cache-update-p
								 "true"
								 "false")))
	      :content (babel:string-to-octets (format nil "(list :query #'~S)" query))))



